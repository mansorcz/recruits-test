.PHONY: console composer execute yarn install-yarn

console:
	docker-compose exec app php /var/www/bin/console $(filter-out $@,$(MAKECMDGOALS))

composer:
	docker-compose exec app composer $(filter-out $@,$(MAKECMDGOALS))

execute:
	docker-compose exec app $(filter-out $@,$(MAKECMDGOALS))

init:
	chmod 777 -R rabbitmq
	chmod 777 -R website/var
	make console messenger:setup-transports

install-yarn:
	docker-compose run node yarn install

yarn:
	docker-compose run node yarn $(filter-out $@,$(MAKECMDGOALS))
node:
	docker-compose run node node $(filter-out $@,$(MAKECMDGOALS))
npm:
	docker-compose run node npm $(filter-out $@,$(MAKECMDGOALS))
npx:
	docker-compose run node npx $(filter-out $@,$(MAKECMDGOALS))
gulp:
	docker-compose run node gulp $(filter-out $@,$(MAKECMDGOALS))

build-yarn-main:
	docker-compose run node yarn main:build
watch-yarn-main:
	docker-compose run node yarn main:watch