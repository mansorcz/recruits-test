<?php

namespace App\Twig;

use App\Component\Component;
use App\Component\Grid\PriceColumn;
use App\Component\Notification\NotificationRenderer;
use App\Component\Subscription\SubscriptionMode;
use App\Component\User\Setting\UserSettingsFacade;
use App\Entity\Notification;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Tier\Image\Component\ImageService;
use Tier\Image\Entity\Image;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CustomExtension extends AbstractExtension
{
    public function __construct(
        private Environment $environment,
        private UrlGeneratorInterface $generator,
        private RequestStack $requestStack,
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('phone', [$this, 'renderPhone'], ['is_safe' => ['all']]),
            new TwigFilter('imagePath', [$this, 'imagePath']),
            new TwigFilter('price', [$this, 'price']),
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('renderComponent', [$this, 'renderComponent'], ['is_safe' => ['all']]),
            new TwigFunction('renderNew', [$this, 'renderNew'], ['is_safe' => ['all']]),
            new TwigFunction('mapLink', [$this, 'mapLink'], ['is_safe' => ['all']]),
            new TwigFunction('toTime', [$this, 'toTime']),
            new TwigFunction('boolToStr', [$this, 'boolToStr']),
            new TwigFunction('componentLink', [$this, 'componentLink']),
            new TwigFunction('dotToKey', [$this, 'dotToKey'], ['is_safe' => ['all']]),
            new TwigFunction('renderNotification', [$this, 'renderNotification'], ['is_safe' => ['all']]),
            new TwigFunction('daysBetween', [$this, 'daysBetween'], ['is_safe' => ['all']]),
            new TwigFunction('getLightMode', [$this, 'getLightMode'], ['is_safe' => ['all']]),
            new TwigFunction('getMenuIsOpened', [$this, 'getMenuIsOpened'], ['is_safe' => ['all']]),
            new TwigFunction('isSubscriptionModeActive', [$this, 'isSubscriptionModeActive'], ['is_safe' => ['all']]),
        ];
    }


    public function renderComponent(object $component): string
    {
        return $component->render();
    }

    public function toTime(int $hour, int $second): string
    {
        return (new \DateTime())->setTime($hour, $second)->format('H:i');
    }

    public function boolToStr(bool $bool): string
    {
        return $bool ? 'true' : 'false';
    }

    public function renderNew(Component $component): string
    {
        if (str_contains($component->getTemplate(), '::')) {
            [$templatePath, $block] = explode('::', $component->getTemplate());
            $template = $this->environment->load($templatePath);
            return $template->renderBlock($block, $component->getContext());
        }
        return $this->environment->render(
            $component->getTemplate(),
            $component->getContext()
        );
    }

    public function componentLink(Component $component, array $parameters): string
    {
        $filteredParameters = [];
        foreach ($parameters as $key => $parameter) {
            $filteredParameters[$component->parameterName($key)] = $parameter;
        }
        $parameterBag = $this->requestStack->getCurrentRequest()->attributes;
        return $this->generator->generate(
            $parameterBag->get('_route'),
            $filteredParameters + $parameterBag->get('_route_params')
        );
    }

    public function renderPhone(?string $phone): string
    {
        if (is_null($phone)) {
            return '';
        }
        return '<a href="tel: ' . $phone . '">' . $phone . '</a>';
    }

    public function mapLink(string $location, float $lat, float $lon): string
    {
        return '<a href="https://www.google.com/maps/dir//' . $lon . ',' . $lat . '/@' . $lon . ',' . $lat . ',14z" target="_blank">' . $location . '</a>';
    }


    public function dotToKey(array|object $row, string $key): mixed
    {
        if ($key === 'this') {
            return $row;
        }
        $pieces = explode('.', $key);
        foreach ($pieces as $piece) {
            if (is_null($row)) {
                return $row;
            }
            if (is_object($row)) {
                $refl = new \ReflectionClass($row);
                if (str_contains($row::class, 'Proxies\\__CG__')) {
                    $refl = new \ReflectionClass($refl->getParentClass()->getName());
                }
                if (!$refl->hasProperty($piece)) {
                    return null;
                }
                $prop = $refl->getProperty($piece);
                $prop->setAccessible(true);
                $row = $prop->getValue($row);
            } else {
                if (!array_key_exists($piece, $row)) {
                    return null;
                }
                $row = $row[$piece];
            }
        }

        return $row;
    }


    public function daysBetween(?\DateTimeInterface $start = null, ?\DateTimeInterface $end = null): int
    {
        if (is_null($start)) {
            $start = new \DateTime();
        }
        if (is_null($end)) {
            $end = new \DateTime();
        }
        return round(($end->getTimestamp() - $start->getTimestamp()) / (60*60*24));
    }

    public function price(float $value, int $floats = 0, string $currency = 'Kč'): string
    {
        return PriceColumn::formatPriceStatic($value, $floats, $currency);
    }
}
