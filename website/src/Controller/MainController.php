<?php

namespace App\Controller;

use App\Component\Api\ApiClient;
use App\Component\Api\Exception\ApiFailedException;
use App\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MainController extends AbstractController
{
    public function __construct(
        private ApiClient $client,
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/', name: 'route.main.index')]
    public function indexAction(Request $request): Response
    {
        $page = $request->get('page', 1);
        $list = $this->getJobs($page);
        if ($request->isXmlHttpRequest()) {
            return new Response($this->getJobs($page));
        }
        return $this->render('jobs.twig', [
            'list' => $list
        ]);
    }

    private function getJobs(int $page, int $perPage = 20): string
    {
        try {
            $pageData = $this->client->getJobs($page, $perPage);
            $jobIds = $pageData['jobIds'];
            $hasNext = $pageData['has_next'];
            $items = $this->entityManager->getRepository(Job::class)->getByJobIds($jobIds)->getQuery()->getResult();
        } catch (ApiFailedException) {
            $items = $this->entityManager->getRepository(Job::class)->getItems($page, $perPage)->getQuery()->getResult();
            $hasNext = !empty($this->entityManager->getRepository(Job::class)->getItems($page + 1, $perPage)->getQuery()->getResult());
        }
        dump($items);
        return $this->renderView('job_list.twig', [
            'items' => $items,
            'has_next' => $hasNext,
            'page' => $page
        ]);
    }
}