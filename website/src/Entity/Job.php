<?php

namespace App\Entity;

use App\Repository\JobRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;

#[Entity(repositoryClass: JobRepository::class)]
class Job
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    #[Column]
    public int $job_id;
    #[Column]
    public string $secured_id;
    #[Column]
    public bool $draft;
    #[Column]
    public bool $active;
    #[Column]
    public int $access_state;
    #[Column]
    public ?int $public_id;
    #[Column]
    public string $title;
    #[Column(type: 'text')]
    public string $description;
    #[Column(type: 'text')]
    public ?string $internal_note;
    #[Column]
    public ?\DateTime $date_end;
    #[Column]
    public ?\DateTime $date_closed;
    #[Column]
    public ?int $closed_duration;
    #[Column]
    public ?\DateTime $date_created;
    #[Column]
    public ?\DateTime $date_created_origin;
    #[Column]
    public ?\DateTime $last_update;
    #[Column]
    public string $text_language;
    #[Column]
    public ?array $workfields;
    #[Column]
    public ?array $filterlist;
    #[Column]
    public ?array $education;
    #[Column]
    public ?bool $disability;
    #[Column]
    public ?array $details;
    #[Column]
    public ?array $personalist;
    #[Column]
    public ?array $contact;
    #[Column]
    public ?array $sharing;
    #[Column]
    public ?array $addresses;
    #[Column]
    public ?array $employment;
    #[Column]
    public bool $confidential;
    #[Column]
    public ?array $salary;
    #[Column]
    public string $salary_default_currency;
    #[Column]
    public ?array $channels;
    #[Column]
    public string $edit_link;
    #[Column]
    public string $public_link;
    #[Column]
    public ?array $referrals;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}