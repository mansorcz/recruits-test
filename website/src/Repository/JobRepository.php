<?php

namespace App\Repository;

use App\Entity\Job;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class JobRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Job::class);
    }

    public function getItems(int $page, int $perPage): QueryBuilder
    {
        $qb = $this->createQueryBuilder('j');
        $qb->orderBy('j.date_created', 'DESC');
        $qb->setMaxResults($perPage)->setFirstResult($perPage * ($page - 1));
        return $qb;
    }

    public function getByJobIds(array $jobIds): QueryBuilder
    {
        $qb = $this->createQueryBuilder('j');
        $qb->where($qb->expr()->in('j.job_id', ':ids'))
            ->setParameter('ids', $jobIds);
        $qb->orderBy('j.date_created', 'DESC');
        return $qb;
    }
}