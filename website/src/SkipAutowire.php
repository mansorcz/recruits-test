<?php

declare(strict_types=1);

namespace App;

use Attribute;
use Symfony\Component\DependencyInjection\Attribute\When;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::TARGET_FUNCTION | Attribute::IS_REPEATABLE)]
final class SkipAutowire extends When
{
    public function __construct()
    {
        parent::__construct('never');
    }
}