<?php

namespace App\Component;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class Component
{
    /** @var array<string, Component>  */
    public static array $components = [];

    private static Request $request;
    protected static UrlGeneratorInterface $urlGenerator;

    private string $name;

    public function __construct(
        string $name
    )
    {
        if (array_key_exists($name, self::$components)) {
            throw new \Exception('Component with "' . $name . '" name already exist');
        }
        $this->name = $name;
        self::$components[$name] = $this;
    }

    public static function setGenerator(UrlGeneratorInterface $generator): void
    {
        self::$urlGenerator = $generator;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public static function setRequest(Request $request): void
    {
        self::$request = $request;
    }

    public function getRequest(): Request
    {
        return self::$request;
    }

    public static function getRequestStatic(): Request
    {
        return self::$request;
    }

    public function parameterName(string $parameter): string
    {
        return $this->getName() . '-' . $parameter;
    }

    public function componentLink(Component $component, array $parameters): string
    {
        $filteredParameters = [];
        foreach ($parameters as $key => $parameter) {
            $filteredParameters[$component->parameterName($key)] = $parameter;
        }
        $parameterBag = self::$request->attributes;
        return self::$urlGenerator->generate(
            $parameterBag->get('_route'),
            $filteredParameters + $parameterBag->get('_route_params')
        );
    }

    public abstract function getTemplate(): string;

    public abstract function getContext(): array;
}