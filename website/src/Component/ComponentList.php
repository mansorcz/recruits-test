<?php

namespace App\Component;

use App\Component\Pagination\Pagination;
use Doctrine\ORM\QueryBuilder;

abstract class ComponentList extends Component
{
    public const PER_PAGE = 10;
    public const PAGE = 1;
    private int $perPage = self::PER_PAGE;
    private int $page = self::PAGE;
    /** @var QueryBuilder|array */
    protected $dataSource;

    private Pagination $pagination;

    public function __construct(string $name, $dataSource)
    {
        parent::__construct($name);
        $this->dataSource = $dataSource;
        $this->processRequest();
    }

    public function getDatasourceClone(): QueryBuilder|array
    {
        return clone $this->dataSource;
    }

    /**
     * @return Pagination
     */
    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function setPagination(Pagination $pagination): void
    {
        $this->pagination = $pagination;
    }

    public function hasPagination(): bool
    {
        return isset($this->pagination);
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    protected function processRequest(): void
    {
        if ($this->hasPagination()) {
            $this->perPage = $this->getRequest()->get($this->parameterName('perPage')) ?? self::PER_PAGE;
            $this->pagination->handleRequest();
            $this->page = $this->pagination->getCurrent();
        }
    }

    protected function applyPagination(): void
    {
        if (!$this->hasPagination()) {
            return;
        }
        if ($this->dataSource instanceof QueryBuilder) {
            $this->dataSource->setMaxResults($this->perPage);
            $this->dataSource->setFirstResult($this->perPage * ($this->page - 1));
        }
    }

    public function setPerPage(int $perPage): self
    {
        $this->perPage = $perPage;
        return $this;
    }
}