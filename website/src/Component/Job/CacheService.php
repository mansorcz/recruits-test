<?php

namespace App\Component\Job;

use App\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;

class CacheService
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function getExistingJob(int $jobId): ?Job
    {
        return $this->entityManager->getRepository(Job::class)->findOneBy(['job_id' => $jobId]);
    }

    public function syncResults(array $results): void
    {
        foreach ($results as $item) {
            $job = $this->getExistingJob($item['job_id']);
            if (!$job instanceof Job) {
                $job = new Job();
                $this->entityManager->persist($job);
            }
            foreach ($item as $key => $value) {
                if (in_array($key, [
                    'date_end',
                    'date_closed',
                    'date_created',
                    'date_created_origin',
                    'last_update'
                ])) {
                    if (!is_null($value)) {
                        try {
                            $value  = new \DateTime($value);
                        } catch (\Throwable) {
                            $value = new \DateTime();
                        }
                    }
                }
                $job->$key = $value;
            }
        }
        $this->entityManager->flush();
    }
}