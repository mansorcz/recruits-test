<?php

namespace App\Component\Job;

use App\Component\Grid\AGrid;
use Doctrine\ORM\QueryBuilder;

class JobGrid extends AGrid
{
    public function __construct(string $name, QueryBuilder $dataSource)
    {
        parent::__construct($name, $dataSource);

    }
}