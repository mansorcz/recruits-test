<?php

namespace App\Component\Api\Exception;

class ApiFailedException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Api failed');
    }
}