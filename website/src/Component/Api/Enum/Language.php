<?php

namespace App\Component\Api\Enum;

enum Language: string
{
    case CS = 'cs';
    case EN = 'en';
    case SK = 'sk';
    case DE = 'de';
    case RO = 'ro';
    case BG = 'bg';
    case HU = 'hu';
    case PL = 'pl';

}