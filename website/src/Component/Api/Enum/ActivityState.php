<?php

namespace App\Component\Api\Enum;

enum ActivityState: int
{
    case ACTIVE = 1;
    case INACTIVE = 2;
    case ALL = 3;
}
