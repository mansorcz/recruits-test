<?php

namespace App\Component\Api\Enum;

enum Rewards: int
{
    case HAS = 1;
    case HAS_NOT = 0;
}
