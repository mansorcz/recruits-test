<?php

namespace App\Component\Api\Enum;

enum Order: string
{
    case DATE_CREATED = 'date_created';
    case DATE_CHANNEL_ASSIGNED = 'date_channel_assigned';
}
