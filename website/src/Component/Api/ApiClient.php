<?php

namespace App\Component\Api;

use App\Component\Api\Enum\AccessState;
use App\Component\Api\Enum\ActivityState;
use App\Component\Api\Enum\Language;
use App\Component\Api\Enum\Order;
use App\Component\Api\Enum\Rewards;
use App\Component\Api\Exception\ApiFailedException;
use App\Component\Job\CacheService;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiClient
{
    private HttpClientInterface $client;

    public function __construct(
        private string $apiUrl,
        private string $token,
        private CacheService $cacheService
    ) {
    }

    private function createRequest(string $method, string $url, array $headers, array $data): ResponseInterface
    {
        $headers['Authorization'] = 'Bearer ' . $this->token;
        return $this->getClient()->request($method, $this->apiUrl . $url, [
            'headers' => $headers,
            'json' => $data
        ]);
    }

    protected function get(string $url, array $data): ResponseInterface
    {
        $query = '';
        if (!empty($data)) {
            $query = '?' . http_build_query($data);
        }
        return $this->createRequest('GET', $url . $query, [], []);
    }

    protected function post(string $url, array $data): ResponseInterface|array
    {
        $response = $this->createRequest('GET', $url, [
            'Content-Type' => 'application/json',
        ], $data);
        if ($response->getStatusCode() === Response::HTTP_OK) {
            return json_decode($response->getContent(), true);
        }
        return $response;
    }

    public function getClient(): HttpClientInterface
    {
        if (isset($this->client)) {
            return $this->client;
        }
        return $this->client = HttpClient::create();
    }

    public function getJobs(
        int $page,
        int $limit = 20,
        Language $language = Language::CS,
        array $workfieldIds = [],
        array $officeIds = [],
        array $filterIds = [],
        array $channelIds = [],
        Order $order = Order::DATE_CREATED,
        ActivityState $activityState = ActivityState::ACTIVE,
        AccessState $accessState = AccessState::OPEN,
        Rewards $rewards = Rewards::HAS_NOT,
        ?\DateTime $updatedFrom = null,
        ?\DateTime $updatedTo = null
    ): array {
        if ($limit > 50) {
            $limit = 50;
        }
        try {

            $response = $this->get('/jobs', [
                'limit' => $limit,
                'page' => $page,
                'text_language' => $language->value,
                'workfield_id' => $workfieldIds,
                'office_id' => $officeIds,
                'filter_id' => $filterIds,
                'channel_id' => $channelIds,
                'order_by' => $order->value,
                'activity_state' => $activityState->value,
                'access_state' => $accessState->value,
                'with_rewards' => $rewards->value,
                'updated_from' => $updatedFrom?->format('Y-m-d H:i:s'),
                'updated_to' => $updatedTo?->format('Y-m-d H:i:s')
            ]);
            if ($response->getStatusCode() !== Response::HTTP_OK) {
                throw new ApiFailedException();
            }
        } catch (\Throwable) {
            throw new ApiFailedException();
        }
        $data = json_decode($response->getContent(), true);
        $this->cacheService->syncResults($data['payload']);

        $jobIds = array_column($data['payload'], 'job_id');
        return [
            'jobIds' => $jobIds,
            'has_next' => $data['meta']['entries_to'] < $data['meta']['entries_total']
        ];
    }
}











