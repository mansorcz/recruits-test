<?php

namespace App\Component\Pagination;

use App\Component\Component;

class Pagination extends Component
{
    const PAGE_PARAMETER = 'page';
    private int $current;
    public function __construct(
        string $name,
        private int $pages,
    )
    {
        parent::__construct($name);
    }

    public function setCurrent(int $current): void
    {
        $this->current = $current;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function hasNext(): bool
    {
        return $this->getCurrent() < $this->pages && $this->pages > 0;
    }
    public function hasPrev(): bool
    {
        return $this->getCurrent() > 1 && $this->pages > 1;
    }

    public function getTemplate(): string
    {
        return 'component/pagination/pagination.twig';
    }

    public function getContext(): array
    {
        return [
            'this' => $this
        ];
    }

    public function handleRequest(): void
    {
        $this->current = $this->getRequest()->get($this->parameterName(self::PAGE_PARAMETER), 1);
    }
}