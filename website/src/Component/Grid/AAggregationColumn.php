<?php

namespace App\Component\Grid;

abstract class AAggregationColumn extends AColumn
{
    protected bool $isAggregationActive = false;
    protected float $operatedValue = 0;

    public abstract function processValue(float|int|null $value): string;

    public function isAggregationColumn(): bool
    {
        return true;
    }

    /**
     * @return float
     */
    public function getOperatedValue(): string
    {
        return $this->operatedValue;
    }

    /**
     * @return bool
     */
    public function isAggregationActive(): bool
    {
        return $this->isAggregationActive;
    }
}