<?php

namespace App\Component\Grid;

class TextColumn extends AColumn
{
    public function __construct(
        string $key,
        string $column,
        string $label,
        private ?int $length = null
    ) {
        parent::__construct($key, $column, $label);
    }

    public function hasLength(): bool
    {
        return !is_null($this->length);
    }

    public function splitText(?string $data): string
    {
        if (is_null($data)) {
            return '';
        }
        $concat = '';
        $length = $this->length;
        if (strlen($data) > $length) {
            $space = strpos($data, ' ', $length);
            if ($space !== false) {
                $length = $space;
                $concat = '...';
            }
        }
        return substr($data, 0, $length) . $concat;
    }
}