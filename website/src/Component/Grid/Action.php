<?php

namespace App\Component\Grid;

use Doctrine\Common\Collections\Collection;

class Action
{
    /** @var string[] */
    protected array $classes = [];

    /** @var callable */
    protected $allowToShow;

    public function __construct(
        private string $key,
        private string $label,
        private string $route,
        private array $routeParams = []
    )
    {
    }

    /**
     * @return string[]
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    public function renderClasses(): string
    {
        return implode(' ', $this->classes);
    }

    public function addClass(string $class): self
    {
        $this->classes[$class] = $class;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function resolveParams(array|object $row): array
    {
        $params = [];
        foreach ($this->routeParams as $key => $path) {
            if (str_starts_with($key, '!')) {
                $params[str_replace('!', '', $key)] = $path;
                continue;
            }
            $params[$key] = $this->dotToKey($row, $path);
        }
        return $params;
    }

    public function dotToKey(array|object $row, string $key): mixed
    {
        if ($key === 'this') {
            return $row;
        }
        $pieces = explode('.', $key);
        foreach ($pieces as $piece) {
            if (is_null($row)) {
                return $row;
            }
            if (is_object($row)) {
                $refl = new \ReflectionClass($row);
                if (str_contains($row::class, 'Proxies\\__CG__')) {
                    $refl = new \ReflectionClass($refl->getParentClass()->getName());
                }
                if (!$refl->hasProperty($piece)) {
                    return null;
                }
                $prop = $refl->getProperty($piece);
                $prop->setAccessible(true);
                $value = $prop->getValue($row);
                if ($value instanceof Collection) {
                    $value = $value->first();
                }
                $row = $value;
            } else {
                if (!array_key_exists($piece, $row)) {
                    return null;
                }
                $row = $row[$piece];
            }
        }

        return $row;
    }

    public function showCondition(callable $callback): self
    {
        $this->allowToShow = $callback;
        return $this;
    }

    public function resolveShow(array|object $row): bool
    {
        if (isset($this->allowToShow)) {
            $temp = $this->allowToShow;
            return $temp($row);
        }
        return true;
    }
}