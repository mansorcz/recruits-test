<?php

namespace App\Component\Grid;

use AllowDynamicProperties;
use App\Component\Pagination\Pagination;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use function Symfony\Component\String\b;

abstract class AGrid extends \App\Component\ComponentList
{

    /** @var AColumn[] */
    protected array $columns = [];

    /** @var Action[] */
    protected array $actions = [];

    /** @var callable */
    protected $rowCallback;

    protected bool $externalFilters = false;

    private bool $hasAggregationColumns = false;

    private array $filters = [];
    private FormInterface $form;

    protected bool $filtersDisabled = false;

    public function __construct(string $name, QueryBuilder|array $dataSource, private bool $disablePagination = false)
    {
        parent::__construct($name, $dataSource);
    }

    public function addColumnText(string $key, string $column, string $label, ?int $length = null): TextColumn
    {
        return $this->addColumn(new TextColumn($key, $column, $label, $length));
    }

    public function addColumnNumber(string $key, string $column, string $label, int $precision = 2): NumberColumn
    {
        return $this->addColumn(new NumberColumn($key, $column, $label, $precision));
    }
    public function addColumnColor(string $key, string $column, string $label): ColorColumn
    {
        return $this->addColumn(new ColorColumn($key, $column, $label));
    }

    public function addColumnPrice(
        string $key,
        string $column,
        string $label,
        string $currency,
        int $floats = 0
    ): PriceColumn {
        return $this->addColumn(new PriceColumn($key, $column, $label, $currency, $floats));
    }

    public function addColumnYesNo(string $key, string $column, string $label): YesNoColumn
    {
        return $this->addColumn(new YesNoColumn($key, $column, $label));
    }

    public function addColumnDate(string $key, string $column, string $label, string $format = 'd.m.Y'): DateColumn
    {
        return $this->addColumn(new DateColumn($key, $column, $label, $format));
    }

    /**
     * @template T
     * @param T $column
     * @return T
     */
    private function addColumn(AColumn $column): AColumn
    {
        if ($column->isAggregationColumn()) {
            $this->hasAggregationColumns = true;
        }
        $this->columns[$column->getKey()] = $column;
        return $column;
    }

    public function addAction(string $key, string $label, string $route, array $routeParams = []): Action
    {
        $action = new Action($key, $label, $route, $routeParams);
        $this->actions[$key] = $action;
        return $action;
    }

    public function getRows(): array
    {
        $this->applyFilters();
        $this->resolvePagination($this->dataSource);
        $this->processRequest();
        $this->applyPagination();
        return $this->dataSource->getQuery()->getResult();
    }

    /**
     * @return  AColumn[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getTemplate(): string
    {
        return 'component/grid/default.twig::grid';
    }

    public function getContext(): array
    {
        return [
            'this' => $this,
            'hasFilters' => $this->hasFilters(),
            'form' => $this->getForm()
        ];
    }

    /**
     * @return Action[]
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    public function hasActions(): bool
    {
        return !empty($this->actions);
    }

    public function rowCallback(mixed $row): string
    {
        if (isset($this->rowCallback)) {
            $callable = $this->rowCallback;
            return $callable($row);
        }

        return '';
    }

    protected function setRowCallback(callable $callback): self
    {
        $this->rowCallback = $callback;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHasAggregationColumns(): bool
    {
        return $this->hasAggregationColumns;
    }

    public function disableAggregationColumns(): self
    {
        $this->hasAggregationColumns = false;
        return $this;
    }

    public function hasExternalFilter(): bool
    {
        return $this->externalFilters;
    }

    public function hasFilters(): bool
    {
        if ($this->filtersDisabled) {
            return false;
        }
        if (empty($this->filters)) {
            foreach ($this->columns as $column) {
                if ($column->hasFilter()) {
                    $this->filters[] = $column;
                }
            }
        }

        return !empty($this->filters);
    }
    public function attachForm(FormInterface $form): self
    {
        $this->form = $form;
        return $this;
    }

    public function getForm(): ?FormView
    {
        if (!$this->hasFilters()) {
            return null;
        }
        $request = $this->getRequest();
        /** @var AColumn $filter */
        foreach ($this->filters as $filter) {
            $field = $filter->createFilterField($this->form);
            $field->setData($request->get($field->getName()));
        }
        $this->form->add('submit', SubmitType::class, [
            'label' => 'Filtruj'
        ]);
        return $this->form->createView();
    }

    protected function applyFilters(): void
    {
        if (!isset($this->form)) {
            return;
        }
        if (!$this->dataSource instanceof QueryBuilder) {
            return;
        }

        /** @var AColumn $column */
        foreach ($this->filters as $column) {
            $field = $this->form->get($column->getFormKey());
            $this->columns[str_replace('column-', '', $field->getName())]->applyFilter($this->dataSource, $field->getData());

        }
    }

    protected function resolvePagination(array|QueryBuilder $dataSource): void
    {
        if (!$this->disablePagination) {
            if ($this->hasPagination()) {
                return;
            }
            $pages = 0;
            if ($dataSource instanceof QueryBuilder) {
                $qbClone = clone $dataSource;
                $rootAliases = $qbClone->getRootAliases();
                $qbClone->select('COUNT(' . $rootAliases[array_key_first($rootAliases)] . ') AS count');
                $items = $qbClone->getQuery()->getSingleResult()['count'];
                $modulo = $items % $this->getPerPage();
                $pages = ($items - $modulo) / $this->getPerPage() + ($modulo > 0 ? 1 : 0);
            }
            $this->setPagination(new Pagination($this->getName() . '_pagination', $pages));
        }
    }
}