<?php

namespace App\Component\Grid;

class NumberColumn extends AAggregationColumn
{
    private const NONE = 'none';
    private const SUM = 'sum';
    private const SUBTRACT = 'subtract';
    private const DIAMETER = 'diameter';
    protected string $operationMode = self::NONE;

    protected int $valuesCount = 0;
    protected float $operatedValue = 0;

    public function __construct(
        string $key,
        string $column,
        string $label,
        private int $precision = 2
    ) {
        parent::__construct($key, $column, $label);
    }

    public function sumValues(): self
    {
        $this->isAggregationActive = true;
        $this->operationMode = self::SUM;
        return $this;
    }

    public function diameterValues(): self
    {
        $this->isAggregationActive = true;
        $this->operationMode = self::DIAMETER;
        return $this;
    }

    public function processValue(float|int|null $value): string
    {
        if (is_null($value)) {
            return '';
        }
        $this->valuesCount++;
        if ($this->operationMode === self::SUM || $this->operationMode === self::DIAMETER) {
            $this->operatedValue = $this->operatedValue + $value;
        }

        return sprintf($this->getFormat(), $value);
    }

    /**
     * @return float
     */
    public function getOperatedValue(): string
    {
        if ($this->operationMode === self::DIAMETER) {
            if ($this->valuesCount === 0) {
                return '';
            }
            return sprintf($this->getFormat(), $this->operatedValue / $this->valuesCount);
        }
        return sprintf($this->getFormat(), $this->operatedValue);
    }

    public function getFormat(): string
    {
        return '%.' . $this->precision . 'f';
    }
}