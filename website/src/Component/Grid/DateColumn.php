<?php

namespace App\Component\Grid;

class DateColumn extends AColumn
{
    public function __construct(string $key, string $column, string $label, private string $format = 'd.m.Y')
    {
        parent::__construct($key, $column, $label);
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }
}