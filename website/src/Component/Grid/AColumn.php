<?php

namespace App\Component\Grid;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

abstract class AColumn
{
    /**
     * @var callable
     */
    private $renderCallback;

    private bool $hasFilter = false;

    public function __construct(
        private string $key,
        private string $column,
        private string $label
    )
    {
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function blockName(): string
    {
        $pieces = explode('\\', $this::class);
        $classNameWithoutNamespace = end($pieces);
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $classNameWithoutNamespace));
    }

    public function setRenderCallback(callable $callback): self
    {
        $this->renderCallback = $callback;
        return $this;
    }

    public function hasRenderCallback(): bool
    {
        return isset($this->renderCallback);
    }

    /**
     * @return callable
     */
    public function render(...$params): string
    {
        $callable = $this->renderCallback;
        return $callable(...$params);
    }

    public function isAggregationColumn(): bool
    {
        return false;
    }

    public function setFilter(): self
    {
        $this->hasFilter = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasFilter(): bool
    {
        return $this->hasFilter;
    }

    public function createFilterField(FormInterface $form): FormInterface
    {
        return $form->add($this->getFormKey(), TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => $this->label
            ]
        ]);
    }

    public function getFormKey(): string
    {
        return 'column-' . $this->key;
    }

    public function applyFilter(QueryBuilder $builder, mixed $data): void
    {
        if (empty($data)) {
            return;
        }
        $column = $this->column;
        if (!str_contains('.', $column))
        {
            $column = $builder->getRootAliases()[array_key_first($builder->getRootAliases())] . '.' .$column;
        }
        $builder->andWhere($column . ' LIKE :param_' . $this->key)->setParameter('param_' . $this->key, '%'.$data.'%');
    }
}