<?php

namespace App\Component\Grid;

class PriceColumn extends AColumn
{
    public function __construct(string $key, string $column, string $label, private string $currency, private int $floats = 0)
    {
        parent::__construct($key, $column, $label);
    }

    public function formatPrice(mixed $value): string
    {
        if (!is_float($value)) {
            $value = floatval($value);
            if (!is_float($value)) {
                return '';
            }
        }
        return number_format(round($value, $this->floats), $this->floats) . ' ' . $this->currency;
    }

    public static function formatPriceStatic(float $value, int $floats, string $currency): string
    {
        return number_format(round($value, $floats), $floats) . ' ' . $currency;
    }
}