FROM php:8.1-fpm AS base

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
#COPY --from=node:16-alpine /usr/bin/npm /usr/local/bin/
RUN install-php-extensions gd xdebug intl

# Install system dependencies
RUN apt-get update && apt-get install -y git
RUN apt-get update && apt-get install -y dh-autoreconf zip libzip-dev
RUN docker-php-ext-install pdo_mysql \
    zip
RUN pecl install redis-5.3.7 \
	&& pecl install xdebug-3.2.1 \
	&& docker-php-ext-enable redis xdebug
RUN docker-php-ext-install pcntl;
# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer self-update
RUN #cd /var/www && composer install
# Set working directory
WORKDIR /var/www

#RUN install-php-extensions amqp

FROM base AS dev
COPY ./docker/nginx/conf.d/ /etc/nginx/conf.d/

EXPOSE 9000